default: setup

.phony: setup clean
setup:
	p=$(shell pwd) && \
	for f in $(shell ls -A config); do \
		ln -sf $$p/config/$$f ..; \
	done

clean:
	for f in $(shell ls -A config); do \
		rm ../$$f; \
	done
