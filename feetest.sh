#!/bin/sh

export TEZOS_LOG='* -> debug'

# set -ex
iterations=${1:-30}
mockup_dir="mckp"
diff_dir="${mockup_dir}/diffs"
chain="NetXynUjJNZm7wi"
context_json=${mockup_dir}/mockup/context.json

client=" ./tezos-client --mode mockup --base-dir $mockup_dir --protocol ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"

rm -Rf $mockup_dir


runclient () {
    cmd="$client $1"
    echo ""
    echo "------ $cmd"
    $cmd
    echo ""
    echo "------"
    echo ""
}

doclient () {
    runclient "$1"
    echo "------ Mempool"
    cat $mockup_dir/mockup/mempool.json
    echo ""
    echo "------"
    echo ""

}

# This is where we setup the fees
fee=1
fees="--fee ${fee} --fee-cap ${fee}"

doclient "create mockup --asynchronous"

mkdir ${diff_dir}


# cp mystuff/context.json ${context_json}


for i in $(seq ${iterations})
do
         doclient "transfer 1 from bootstrap1 to bootstrap2 ${fees}";
         echo "Transfer $i"
         doclient "bake for bootstrap4 --minimal-timestamp"
         # See the differences
         # if [[ -f ${diff_dir}/context.json ]]
         # then
         #     diff ${diff_dir}/context.json ${context_json} > ${diff_dir}/${i}.diff
         # fi
         # cp ${context_json} ${diff_dir}/context.json

         runclient "get balance for bootstrap4"
done
