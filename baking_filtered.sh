#!/bin/sh

set -e
mockup_dir="mckp"
chain="NetXynUjJNZm7wi"
client=" ./tezos-client --mode mockup --base-dir $mockup_dir --protocol ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"

rm -Rf $mockup_dir

runclient () {
    cmd="$client $1"
    echo ""
    echo "------ $cmd"
    $cmd
    echo ""
    echo "------"
    echo ""
}

doclient () {
    runclient "$1"
    echo "------ Mempool"
    cat $mockup_dir/mockup/mempool.json
    echo ""
    if [[ -f $mockup_dir/mockup/trashpool.json ]]
    then
        echo "------ Trashpool"
        cat $mockup_dir/mockup/trashpool.json
        echo ""
    fi
    echo "------"
    echo ""
}

pending_operations () {
    doclient "rpc get /chains/$chain/mempool/pending_operations"
}

balance () {
    runclient "get balance for $1"
}

all_balances () {
    for i in `seq 1 5`
    do
        balance "bootstrap$i"
    done
}

live_blocks () {
    head_block_hash=$(cat $mockup_dir/mockup/context.json| jq '.context.shell_header.predecessor' | tr -d '\"')
    chain_id=$(cat $mockup_dir/mockup/context.json| jq '.chain_id' | tr -d '\"')
    doclient "rpc get /chains/$chain_id/blocks/$head_block_hash/live_blocks"
}

doclient "create mockup --asynchronous"

doclient "transfer 1 from bootstrap1 to bootstrap2 --fee 1"
doclient "transfer 2 from bootstrap2 to bootstrap3 --fee 0.5"

# pending_operations
# live_blocks

# Shouldn't have changed
runclient "get balance for bootstrap1"
runclient "get balance for bootstrap2"

doclient "bake for bootstrap1 --minimal-timestamp --minimal-fees 0.6"

# live_blocks

# all_balances

# pending_operations

doclient "transfer 1 from bootstrap4 to bootstrap5 --fee 1" # no fee
doclient "transfer 2 from bootstrap2 to bootstrap3 --fee 0.5"


sleep 2
doclient "bake for bootstrap3 --minimal-fees 0.6"
live_blocks
all_balances


sleep 2
doclient "bake for bootstrap3"
live_blocks
all_balances

# for i in $(seq 5)
# do
#          doclient "transfer 1 from bootstrap1 to bootstrap2";
#          echo "Transfer $i"
#          sleep 2;
#          doclient "bake for bootstrap1"
# done
