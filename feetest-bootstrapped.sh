#!/bin/sh

eval `./src/bin_client/tezos-init-sandboxed-client.sh 1`

user=bootstrap3
baker=bootstrap4
v=0

bake_transfer () {
    tezos-client transfer 1 from bootstrap1 to bootstrap2 --fee 1 2>/dev/null 1>/dev/null &
    tezos-client bake for $1 --minimal-timestamp
    # echo $user                  #
    # tezos-client get balance for $user 2>/dev/null
    # echo $1
    # b1=$(tezos-client get balance for $1 2>/dev/null | cut -d' ' -f 1)
    # echo $b1
    # variation=$(echo "$b1 - $v" | bc)
    # echo $variation
    # v=$b1
}


tezos-activate-alpha

for i in $(seq 30)
do
    echo "---- $i"
    bake_transfer $baker
done
