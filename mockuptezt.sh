#!/bin/sh

set -e
mockup_dir="mckp"
chain="NetXynUjJNZm7wi"
client=" ./tezos-client --mode mockup --base-dir $mockup_dir --protocol ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"

rm -Rf $mockup_dir

runclient () {
    cmd="$client $1"
    echo ""
    echo "------ $cmd"
    $cmd
    echo ""
    echo "------"
    echo ""
}

doclient () {
    runclient "$1"
    echo "------ Mempool"
    cat $mockup_dir/mockup/mempool.json
    echo ""
    echo "------"
    echo ""
}


doclient "create mockup --asynchronous"

baker="bootstrap1"
for amount in $(seq 1 20)
do
    doclient "transfer $amount from bootstrap1 to bootstrap2"
    doclient "transfer $amount from bootstrap2 to bootstrap1"

    if [ "$baker" = "bootstrap1" ]
    then
        parity="bootstrap2"
    else
        parity="bootstrap1"
    fi
    doclient "bake for $baker --minimal-timestamp"

    runclient "get balance for bootstrap1"
    runclient "get balance for bootstrap2"
done



# doclient "rpc get /chains/$chain/mempool/pending_operations"

# # Shouldn't have changed
# runclient "get balance for bootstrap1"
# runclient "get balance for bootstrap2"

# doclient "bake for bootstrap1 --minimal-timestamp --minimal-fees 0.6"

# runclient "get balance for bootstrap1"
# runclient "get balance for bootstrap2"

# doclient "transfer 1 from bootstrap2 to bootstrap1"

# sleep 2
# doclient "bake for bootstrap1"

# runclient "get balance for bootstrap1"
# runclient "get balance for bootstrap2"

# for i in $(seq 5)
# do
#          doclient "transfer 1 from bootstrap1 to bootstrap2";
#          echo "Transfer $i"
#          sleep 2;
#          doclient "bake for bootstrap1"
# done
