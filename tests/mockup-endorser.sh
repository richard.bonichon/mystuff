#!/bin/sh

# export TEZOS_LOG='* -> debug'

# set -ex
iterations=${1:-30}
mockup_dir="mckp"
chain="NetXynUjJNZm7wi"

client=" ./tezos-client --mode mockup --base-dir $mockup_dir --protocol ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"

rm -Rf $mockup_dir

runclient () {
    cmd="$client $1"
    echo ""
    echo "------ $cmd"
    $cmd
    ret=$?
    echo ""
    echo "------"
    echo ""
    return $ret
}

doclient () {
    runclient "$1"
    echo "------ Mempool"
#    cat $mockup_dir/mockup/mempool.json
    echo ""
    echo "------"
    echo ""
}

bake_for () {
    runclient "bake for $1 --minimal-timestamp"
}

doclient "create mockup --asynchronous"

baker_key="unencrypted:edsk31vznjHSSpGExDMHYASz45VZqXN4DPxvsa4hAyY8dHM28cZzp6"

runclient "import secret key baker ${baker_key}"

runclient "transfer 2000000 from bootstrap2 to baker --burn-cap 0.257 "
runclient "transfer 2000000 from bootstrap1 to baker --burn-cap 0.257 "
bake_for bootstrap2

runclient "register key baker as delegate"

for i in $(seq 1000); do
    runclient "transfer 1 from bootstrap3 to bootstrap4"
    bake_for baker
    ret="$?"
    if [[ "$ret" != "0" ]]
    then
        echo "Baker cannot bake at $i ($?)"
        bake_for bootstrap3
    else
        echo "Done at $i : $ret"
        break
    fi
done

runclient "transfer 1 from bootstrap4 to boostrap5"
bake_for "bootstrap3"

runclient "endorse for baker"
