#!/bin/sh

#!/bin/sh

export TEZOS_LOG='* -> debug'

# set -ex
iterations=${1:-30}
mockup_dir="mckp"
chain="NetXynUjJNZm7wi"

client=" ./tezos-client --mode mockup --base-dir $mockup_dir --protocol ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"

rm -Rf $mockup_dir

set -e
runclient () {
    cmd="$client $1"
    echo ""
    echo "------ $cmd"
    $cmd
    echo ""
    echo "------"
    echo ""
}

doclient () {
    runclient "$1"
    echo "------ Mempool"
#    cat $mockup_dir/mockup/mempool.json
    echo ""
    echo "------"
    echo ""
}

doclient "create mockup --asynchronous"

doclient "originate contract take_bytes transferring 1 from bootstrap1 running parameter bytes; storage unit; code {CDR; NIL operation; PAIR}
 --burn-cap 1"

doclient "bake for bootstrap2"

doclient "transfer 0 from bootstrap3 to take_bytes --arg '0x$(python -c print\(\"00\"*17000\))'"
