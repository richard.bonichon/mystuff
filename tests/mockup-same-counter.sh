#!/bin/sh

# export TEZOS_LOG='* -> debug'

# set -ex
iterations=${1:-30}
mockup_dir="mckp"
chain="NetXynUjJNZm7wi"

client=" ./tezos-client --mode mockup --base-dir $mockup_dir --protocol ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"

rm -Rf $mockup_dir

runclient () {
    cmd="$client $1"
    echo ""
    echo "------ $cmd"
    $cmd
    ret=$?
    echo ""
    echo "------"
    echo ""
    return $ret
}

doclient () {
    runclient "$1"
    echo "------ Mempool"
    cat $mockup_dir/mockup/mempool.json
    echo ""
    echo "------"
    echo ""
    grep block_hash $mockup_dir/mockup/context.json
    echo ""
    echo "------"
    echo ""
}

bake_for () {
    runclient "bake for $1 --minimal-timestamp"
}

doclient "create mockup --asynchronous"

doclient "transfer 1 from bootstrap2 to bootstrap3"
doclient "transfer 1 from bootstrap2 to bootstrap3"

# doclient "transfer 2 from bootstrap3 to bootstrap4"

bake_for "bootstrap1"

grep block_hash $mockup_dir/mockup/context.json

cat $mockup_dir/mockup/thrashpool.json
