((nil . ((projectile-project-compilation-cmd . "nix-shell --run make")
         (projectile-project-test-cmd . "pytest -s  tests_python/tests/test_mockup.py")))
 ;; Format caml code on save
 (tuareg-mode (mode . ocaml-format-on-save))
 )
