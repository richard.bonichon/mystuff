{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs { overlays = [ (import sources.nixpkgs-mozilla) ]; }
}:
let
  rust = import ./nix/rust.nix {
    inherit sources;
  };
  poetry2nix = import
    (
      builtins.fetchGit {
        url = https://github.com/nix-community/poetry2nix;
        rev = "53d5786a0a9b64745713d05d897c84cc9ef2c9da";
        #        rev = "6e2a8ce9cc7a2f9d66a8d513fd919d47ac8c24c0";
      }
    )
    { poetry = pkgs.poetry; };

  # for some reason, there are 2 distinct poetry sandboxes
  # one for tests, one for documentation

  #
  testsEnv = poetry2nix.mkPoetryPackages {
    projectDir = ./tests_python;
    overrides =
      poetry2nix.overrides.withDefaults
        (
          self: super: {
            # Fixes https://github.com/pytest-dev/pytest/issues/7891
            pytest = super.pytest.overrideAttrs (old: {
              postPatch = old.postPatch or "" + ''
                sed -i '/\[metadata\]/aversion = ${old.version}' setup.cfg
              '';
            });
          }
        );
  };

  # I've changed the sphinx-rtd-theme version to not track 0.5.0 due to
  # https://github.com/readthedocs/sphinx_rtd_theme/issues/946
  # I've pinned it to 0.4.3, which is plenty sufficient.
  docsEnv = poetry2nix.mkPoetryPackages {
    projectDir = ./docs;
  };


in
rec {
  devshell = pkgs.mkShell {
    buildInputs = with pkgs;
      [
        rust
        # Dependencies for Opam packages
        libffi
        gmp
        hidapi
        libev
        perl
        pkg-config
        which
        m4
        zlib
        # End
        opam
        git
        openssl
        poetry
        gitAndTools.lab
        autoconf
      ]
      # ++ testsEnv.poetryPackages
      # ++ docsEnv.poetryPackages
    ;

    SOURCE_DATE_EPOCH = 315532800;

  };
}
