#!/bin/sh
# Reset the devel environment for the Tezos git repository.
# It needs to be run at the toplevel of said repository
# It assumes the direnv + nix environments are already setup

set -e

if [[ -f .envrc ]]
then
    echo "Disabling direnv ..."
    direnv deny
fi

echo "Unloading previously loaded confiuration .. "
dir=$(pwd)
cd $HOME
cd $dir

echo "Resetting development environment ..."
rm -Rf _opam

nix-shell --run "make build-deps"

nix-shell --run "make build-dev-deps"

if [[ -f .envrc ]]
then
    echo "Enabling direnv ..."
    direnv allow
fi
